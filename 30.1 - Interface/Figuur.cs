﻿namespace _30._1___Interface
{
    internal class Figuur : InterfaceObject
    {
        private double _lengte;
        private double _breedte;
        public Figuur(double lengte, double breedte)
        {
            Lengte = lengte;
            Breedte = breedte;
        }
        public double Breedte
        {
            get { return _breedte; }
            set { _breedte = value; }
        }
        public double Lengte
        {
            get { return _lengte; }
            set { _lengte = value; }
        }
        public double Bereken()
        {
            return Lengte * Breedte;
        }
        public override string ToString()
        {
            return "De oppervlakte van de figuur met hoogte " + Lengte + "cm en breedte " + Breedte + "cm is " + Bereken().ToString() + "cm².";
        }
    }
}
