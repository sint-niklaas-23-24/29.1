﻿namespace _30._1___Interface
{
    internal class Bankrekening : InterfaceObject
    {
        private double _rentevoet;
        private double _saldo;

        public Bankrekening(double rentevoet, double saldo)
        {
            _rentevoet = rentevoet;
            _saldo = saldo;
        }

        public double Rentevoet
        {
            get { return _rentevoet; }
            set { _rentevoet = value; }
        }

        public double Saldo
        {
            get { return _saldo; }
            set { _saldo = value; }
        }

        public double Bereken()
        {
            return Rentevoet / 100 * Saldo;
        }

        public override string ToString()
        {
            return $"De intrest van de rekening met saldo: € {Saldo} en rentevoet {Rentevoet}% is {Bereken()} euro.";
        }

    }
}
