﻿using System.Windows;

namespace _30._1___Interface
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        Bankrekening mijneFortis;
        Figuur figuurtje;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            mijneFortis = new Bankrekening(2.3, 1000);
            figuurtje = new Figuur(30, 10);
        }

        private void btnBerekenInterest_Click(object sender, RoutedEventArgs e)
        {
            Bereken(mijneFortis);
        }

        private void btnOppervlakte_Click(object sender, RoutedEventArgs e)
        {
            Bereken(figuurtje);
        }
        private void Bereken(InterfaceObject interfaceObject)
        {
            MessageBox.Show(interfaceObject.ToString());
        }
    }
}